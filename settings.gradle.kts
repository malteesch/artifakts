rootProject.name = "artifakts"

dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            version("kotlin", "2.0.0")
            version("kotest", "5.9.0")
            version("clikt", "4.4.0")
            version("mordant", "2.7.2")
            version("hoplite", "2.7.5")
            version("ktor", "2.3.12")
            version("detekt", "1.23.6")
            version("arrow", "1.2.4")

            plugin("kotlin-jvm", "org.jetbrains.kotlin.jvm").versionRef("kotlin")
            plugin("detekt", "io.gitlab.arturbosch.detekt").versionRef("detekt")

            bundle(
                "kotest", listOf(
                    "kotest-runner-junit5-jvm",
                    "kotest-assertions-core-jvm"
                )
            )
            bundle(
                "hoplite", listOf(
                    "hoplite-core",
                    "hoplite-yaml",
                    "hoplite-json",
                )
            )

            bundle(
                "ktor-client", listOf(
                    "ktor-client-core",
                    "ktor-client-cio",
                    "slf4j-nop"
                )
            )

            bundle(
                "ktor-server", listOf(
                    "ktor-server-core",
                    "ktor-server-cio"
                )
            )

            bundle(
                "arrow", listOf(
                    "arrow",
                    "arrow-fx"
                )
            )

            library("clikt", "com.github.ajalt.clikt", "clikt").versionRef("clikt")
            library("mordant", "com.github.ajalt.mordant", "mordant").versionRef("mordant")
            library("mordant-coroutines", "com.github.ajalt.mordant", "mordant-coroutines")
                .versionRef("mordant")

            library("kotest-runner-junit5-jvm", "io.kotest", "kotest-runner-junit5-jvm")
                .versionRef("kotest")
            library("kotest-assertions-core-jvm", "io.kotest", "kotest-assertions-core-jvm")
                .versionRef("kotest")
            library(
                "kotest-extensions-mockserver",
                "io.kotest.extensions",
                "kotest-extensions-mockserver"
            ).version("1.2.1")
            library(
                "kotest-extensions-arrow",
                "io.kotest.extensions",
                "kotest-assertions-arrow"
            ).version("1.3.3")

            library("hoplite-core", "com.sksamuel.hoplite", "hoplite-core").versionRef("hoplite")
            library("hoplite-yaml", "com.sksamuel.hoplite", "hoplite-yaml").versionRef("hoplite")
            library("hoplite-json", "com.sksamuel.hoplite", "hoplite-json").versionRef("hoplite")

            library("ktor-server-core", "io.ktor", "ktor-server-core").versionRef("ktor")
            library("ktor-server-cio", "io.ktor", "ktor-server-cio").versionRef("ktor")
            library("ktor-client-core", "io.ktor", "ktor-client-core").versionRef("ktor")
            library("ktor-client-cio", "io.ktor", "ktor-client-cio-jvm").versionRef("ktor")
            library("slf4j-nop", "org.slf4j", "slf4j-nop").version("2.0.7")
            library("logback-classic", "ch.qos.logback", "logback-classic").version("1.2.11")

            library("jimfs", "com.google.jimfs", "jimfs").version("1.3.0")

            library("detekt-formatting", "io.gitlab.arturbosch.detekt", "detekt-formatting")
                .versionRef("detekt")
            library("detekt-arrow", "com.wolt.arrow.detekt", "rules").version("0.3.0")

            library("arrow", "io.arrow-kt", "arrow-core").versionRef("arrow")
            library("arrow-fx", "io.arrow-kt", "arrow-fx-coroutines").versionRef("arrow")
        }
    }
}
