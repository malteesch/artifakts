package dev.malteesch.artifakts

import dev.malteesch.artifakts.commands.RootCommand
import java.nio.file.Paths

fun main(args: Array<String>) = RootCommand(Paths.get(System.getProperty("java.io.tmpdir")).resolve("artifakts"))
    .main(args)
