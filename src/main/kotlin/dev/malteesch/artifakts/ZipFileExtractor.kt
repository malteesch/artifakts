package dev.malteesch.artifakts

import arrow.core.Either
import arrow.core.left
import arrow.core.raise.catch
import arrow.core.raise.either
import arrow.core.right
import dev.malteesch.artifakts.error.ArtifaktsError
import dev.malteesch.artifakts.error.IOError
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import kotlin.io.path.absolutePathString

fun extractZipFile(srcPath: Path, destPath: Path): Either<ArtifaktsError, Path> = either {
    return catch({
        ZipInputStream(Files.newInputStream(srcPath)).use { zis ->
            var zipEntry = zis.nextEntry

            while (zipEntry != null) {
                val isDir = zipEntry.name.endsWith(File.separator)
                val newPath = zipSlipProtect(zipEntry, destPath).bind()

                if (isDir) {
                    Files.createDirectories(newPath)
                } else {
                    if (newPath.parent != null) {
                        if (Files.notExists(newPath.parent)) {
                            Files.createDirectories(newPath.parent)
                        }
                    }

                    Files.copy(zis, newPath, StandardCopyOption.REPLACE_EXISTING)
                }
                zipEntry = zis.nextEntry
            }
            zis.closeEntry()
        }
        return destPath.right()
    }) {
        IOError(it.message ?: "Error extracting file ${destPath.absolutePathString()}").left()
    }
}

private fun zipSlipProtect(entry: ZipEntry, targetDir: Path): Either<ArtifaktsError, Path> = either {
    val targetResolved = targetDir.resolve(entry.name)
    val normalizedPath = targetResolved.normalize()
    return if (!normalizedPath.startsWith(targetDir)) {
        raise(IOError("Bad zip entry: %s".format(entry.name)))
    } else {
        normalizedPath.right()
    }
}
