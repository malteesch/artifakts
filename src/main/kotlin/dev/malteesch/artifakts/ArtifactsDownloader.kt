package dev.malteesch.artifakts

import arrow.core.Either
import arrow.core.left
import arrow.core.raise.catch
import arrow.core.raise.either
import arrow.core.right
import dev.malteesch.artifakts.config.Host
import dev.malteesch.artifakts.error.*
import dev.malteesch.artifakts.job.GitLabJob
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.NotFound
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.absolutePathString
import kotlin.time.Duration.Companion.seconds

private const val GITLAB_JOB_URL_FORMAT_STRING = "/api/v4/projects/%s/jobs/%s/artifacts"
private const val DEFAULT_REQUEST_TIMEOUT_SECONDS = 1L
private const val REQUEST_TIMEOUT_SECONDS = 60L

suspend fun downloadJobArtifacts(
    job: GitLabJob,
    destinationDirectory: Path,
    accessToken: String,
    progressUpdate: (current: Long, total: Long) -> Unit = { _, _ -> }
): Either<ArtifaktsError, Path> = either {
    val destFile = destinationDirectory.resolve("${job.jobId}.zip")
    if (Files.exists(destFile)) {
        return@either destFile
    }
    val url = job.jobUrl
    val urlBuilder = URLBuilder(url)
    urlBuilder.path(
        GITLAB_JOB_URL_FORMAT_STRING.format(job.projectIdentifier.encodeURLPathPart(), job.jobId)
    )
    val downloadUrl = urlBuilder.build()

    val response = http.get(downloadUrl) {
        header("Authorization", "Bearer $accessToken")
        onDownload(progressUpdate)
        timeout {
            requestTimeoutMillis = REQUEST_TIMEOUT_SECONDS.seconds.inWholeMilliseconds
        }
    }
    when (response.status) {
        Unauthorized -> raise(NotAuthenticated(Host(url.host)))
        Forbidden -> raise(MissingPermissions(url))
        NotFound -> raise(NoJobFound(url))
    }
    val bytes = response.body<ByteArray>()
    return catch({
        Files.createDirectories(destinationDirectory)
        Files.write(destFile, bytes).right()
    }) { throwable: Throwable ->
        IOError(throwable.message ?: "Error writing file ${destFile.absolutePathString()}", throwable).left()
    }
}

private val http: HttpClient = HttpClient(CIO).config {
    install(HttpTimeout) {
        requestTimeoutMillis = DEFAULT_REQUEST_TIMEOUT_SECONDS.seconds.inWholeMilliseconds
    }
}
