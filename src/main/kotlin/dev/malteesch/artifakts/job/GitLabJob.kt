package dev.malteesch.artifakts.job

import arrow.core.Either
import arrow.core.left
import arrow.core.raise.catch
import arrow.core.raise.either
import arrow.core.right
import dev.malteesch.artifakts.error.ArtifaktsError
import dev.malteesch.artifakts.error.InvalidJobUrl
import io.ktor.http.*

fun parseGitLabJobUrl(jobUrl: Url): Either<ArtifaktsError, GitLabJob> = either {
    return catch({
        val (projectIdentifier, jobId) = jobUrl.fullPath
            .split("/-/jobs")
            .map(::trimSlashes)
        GitLabJob(projectIdentifier, jobId, jobUrl).right()
    }) {
        InvalidJobUrl(jobUrl).left()
    }
}

data class GitLabJob(
    val projectIdentifier: String,
    val jobId: String,
    val jobUrl: Url
)

private fun trimSlashes(string: String): String = string.trim('/')
