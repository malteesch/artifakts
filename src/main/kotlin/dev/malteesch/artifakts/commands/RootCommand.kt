package dev.malteesch.artifakts.commands

import arrow.core.Either
import arrow.core.raise.either
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.core.UsageError
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.mordant.animation.coroutines.animateInCoroutine
import com.github.ajalt.mordant.widgets.progress.percentage
import com.github.ajalt.mordant.widgets.progress.progressBar
import com.github.ajalt.mordant.widgets.progress.progressBarLayout
import com.github.ajalt.mordant.widgets.progress.text
import dev.malteesch.artifakts.config.*
import dev.malteesch.artifakts.downloadJobArtifacts
import dev.malteesch.artifakts.error.ArtifaktsError
import dev.malteesch.artifakts.extractZipFile
import dev.malteesch.artifakts.findHypermediaArtifacts
import dev.malteesch.artifakts.job.GitLabJob
import dev.malteesch.artifakts.job.parseGitLabJobUrl
import dev.malteesch.artifakts.staticContentServer
import io.ktor.http.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.name

class RootCommand(private val downloadDirectory: Path) : CliktCommand(name = "art") {

    private val url: String by argument()
    private val accessToken: String? by option(
        "-t",
        "--token",
        "--access-token",
        help = "Your GitLab access token",
        metavar = "TOKEN"
    )

    override fun run() {
        either {
            val configFile = loadConfig(
                listOf(
                    Source.File(Path(System.getProperty("user.home")).resolve(".artifakts.yml")),
                    Source.File(Path(System.getProperty("user.home")).resolve(".artifakts.yaml")),
                    Source.File(Path(System.getProperty("user.home")).resolve(".artifakts.json")),
                )
            ).bind()

            val config = Config(configFile, if (accessToken != null) Token(accessToken!!) else null)

            val gitLabJob = parseGitLabJobUrl(Url(url)).bind()

            val downloadedFile = runBlocking {
                download(gitLabJob, config, downloadDirectory)
            }.bind()

            val extractedDirectory = extractZipFile(
                downloadedFile,
                downloadedFile.parent.resolve(downloadedFile.name.removeSuffix(".zip"))
            ).bind()

            // TODO remove suppression
            @Suppress("UnusedPrivateProperty")
            val foundIndexFiles = findHypermediaArtifacts(
                extractedDirectory,
                listOf(
                    "index.html",
                    "index.htm"
                ) + config.configFile.additionalIndexFileNames.map(IndexFileName::string)
            ).bind()

            val servers = foundIndexFiles.map { indexFile ->
                staticContentServer(indexFile).map {
                    {
                        val path = indexFile.parent.absolutePathString()
                        val port = it.environment.connectors[0].port
                        currentContext.terminal.info("Serving $path on http://localhost:$port")
                        it.start(wait = true)
                    }
                }
            }.bindAll()

            runBlocking {
                val jobs = servers.map {
                    launch {
                        it.invoke()
                    }
                }
                launch {
                    currentContext.terminal.info("Press Ctrl + C to stop...")
                }
                jobs.joinAll()
            }
        }.onLeft { error: ArtifaktsError ->
            throw CliktError(error.toDisplayMessage())
        }
    }
}

context(CoroutineScope)
private suspend fun RootCommand.download(
    parsedUrl: GitLabJob,
    config: Config,
    downloadDirectory: Path
): Either<ArtifaktsError, Path> = either {
    val host = parsedUrl.jobUrl.host
    val tokenFromCommandLine = config.commandLineToken?.string
    val tokenFromConfigFile = config.configFile.getToken(Host(host))
    val progress = progressBarLayout {
        text("Downloading")
        percentage()
        progressBar()
    }.animateInCoroutine(currentContext.terminal)

    launch { progress.execute() }
    val progressUpdate = { current: Long, t: Long ->
        progress.update {
            completed = current
            total = t
        }
    }

    return downloadJobArtifacts(
        parsedUrl,
        downloadDirectory,
        tokenFromCommandLine ?: tokenFromConfigFile ?: throw UsageError("No token found for host: $host"),
        progressUpdate
    ).also {
        progress.stop()
        progress.clear()
    }
}
