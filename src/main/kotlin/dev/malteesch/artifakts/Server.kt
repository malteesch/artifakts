package dev.malteesch.artifakts

import arrow.core.Either
import arrow.core.raise.either
import dev.malteesch.artifakts.error.ArtifaktsError
import dev.malteesch.artifakts.error.IOError
import io.ktor.server.application.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.server.http.content.*
import io.ktor.server.routing.*
import java.nio.file.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.isDirectory
import kotlin.io.path.name
import kotlin.io.path.notExists

const val FIRST_PORT = 8090

fun staticContentServer(path: Path): Either<ArtifaktsError, ApplicationEngine> = either {
    if (path.notExists()) raise(IOError("File ${path.absolutePathString()} does not exist"))
    if (path.isDirectory()) raise(IOError("Expected a file, got a directory: ${path.absolutePathString()}"))
    embeddedServer(CIO, port = ports.next(), module = staticModule(path))
}

private fun staticModule(path: Path): Application.() -> Unit = {
    routing {
        staticFiles("/", dir = path.parent.toFile(), index = path.fileName.name)
    }
}

private val ports = iterator {
    var next = FIRST_PORT
    while (true) {
        yield(next)
        ++next
    }
}
