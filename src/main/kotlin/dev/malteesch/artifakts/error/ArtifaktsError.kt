package dev.malteesch.artifakts.error

import dev.malteesch.artifakts.config.Host
import io.ktor.http.*

sealed class ArtifaktsError {

    fun toDisplayMessage(): String = when (this) {
        is IOError -> message
        is NoJobFound -> "No job found at url: $url."
        is NotAuthenticated -> "Could not authenticate with provided token."
        is MissingPermissions -> "You aren't allowed to download the artifacts."
        is ConfigError -> "Error loading config: $message."
        is InvalidJobUrl -> "URL was not a valid GitLab job URL ($url)."
    }
}

data class NoJobFound(val url: Url) : ArtifaktsError()
data class MissingPermissions(val url: Url) : ArtifaktsError()
data class NotAuthenticated(val host: Host) : ArtifaktsError()
data class IOError(val message: String, val cause: Throwable? = null) : ArtifaktsError()
data class InvalidJobUrl(val url: Url) : ArtifaktsError()
data class ConfigError(val message: String) : ArtifaktsError()
