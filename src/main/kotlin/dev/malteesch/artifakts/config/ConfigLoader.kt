package dev.malteesch.artifakts.config

import arrow.core.Either
import arrow.core.raise.either
import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.PropertySource
import com.sksamuel.hoplite.addPathSource
import com.sksamuel.hoplite.fp.getOrElse
import dev.malteesch.artifakts.error.ArtifaktsError
import java.nio.file.Path

fun loadConfig(sources: List<Source>): Either<ArtifaktsError, ConfigFile> = either {
    val builder = ConfigLoaderBuilder.default()
    for (source in sources) {
        when (source) {
            is Source.Property -> builder.addSource(source.propertySource)
            is Source.File -> builder.addPathSource(source.path, optional = true)
        }
    }

    builder.build().loadConfig<ConfigFile>().getOrElse { ConfigFile(emptyMap()) }
}

sealed interface Source {
    @JvmInline
    value class Property(val propertySource: PropertySource) : Source

    @JvmInline
    value class File(val path: Path) : Source
}
