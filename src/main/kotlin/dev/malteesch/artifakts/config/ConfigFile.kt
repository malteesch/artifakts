package dev.malteesch.artifakts.config

data class ConfigFile(
    val instances: Map<Host, Token>,
    val additionalIndexFileNames: List<IndexFileName> = listOf()
) {
    fun getToken(host: Host): String? = instances[host]?.string
}

@JvmInline
value class Token(val string: String)

@JvmInline
value class Host(val string: String)

@JvmInline
value class IndexFileName(val string: String)
