package dev.malteesch.artifakts.config

data class GitLab(
    val host: String,
    val token: String
)
