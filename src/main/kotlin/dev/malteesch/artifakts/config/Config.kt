package dev.malteesch.artifakts.config

data class Config(
    val configFile: ConfigFile,
    val commandLineToken: Token?
)
