package dev.malteesch.artifakts

import arrow.core.Either
import arrow.core.raise.catch
import arrow.core.raise.either
import dev.malteesch.artifakts.error.ArtifaktsError
import dev.malteesch.artifakts.error.IOError
import java.io.IOException
import java.nio.file.FileVisitResult
import java.nio.file.FileVisitor
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import kotlin.io.path.absolute
import kotlin.io.path.absolutePathString

fun findHypermediaArtifacts(path: Path, fileNamesToLookFor: List<String>): Either<ArtifaktsError, List<Path>> = either {
    catch({
        val visitor = Visitor(fileNamesToLookFor)
        Files.walkFileTree(path, visitor)
        visitor.indexFiles.toList()
    }) { t -> raise(IOError("Error while searching index files", t)) }
}

private class Visitor(private val fileNamesToLookFor: List<String>) : FileVisitor<Path> {
    val indexFiles: Set<Path> = mutableSetOf()

    override fun preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult {
        val dirHasMatchedFile = indexFiles
            .map(Path::absolute)
            .map(Path::getParent)
            .any { dir.startsWith(it) }
        return if (dirHasMatchedFile) FileVisitResult.SKIP_SUBTREE else FileVisitResult.CONTINUE
    }

    override fun visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult {
        fileNamesToLookFor.forEach {
            if (file.fileName?.toString()?.equals(it) == true) {
                (indexFiles as MutableSet<Path>).add(file)
                return FileVisitResult.CONTINUE
            }
        }
        return FileVisitResult.CONTINUE
    }

    override fun visitFileFailed(file: Path, exc: IOException?): FileVisitResult =
        throw exc ?: IOException("Error processing file ${file.absolutePathString()}")

    override fun postVisitDirectory(dir: Path, exc: IOException?): FileVisitResult {
        return FileVisitResult.CONTINUE
    }
}
