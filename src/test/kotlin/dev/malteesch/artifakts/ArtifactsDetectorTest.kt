package dev.malteesch.artifakts

import com.google.common.jimfs.Jimfs
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import java.nio.file.Files
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString

class ArtifactsDetectorTest : FunSpec({

    test("finds hypermedia index files in a tree of directories") {
        // PREP
        val fs = Jimfs.newFileSystem()
        val zipFile = fs.getPath("/zipfile.zip")
        loadLocalTestFileIntoVirtualFs(Path("dev/malteesch/artifakts/build_dir_with_junit_report.zip"), zipFile)
            .shouldBeRight()
        val unzippedPath = fs.getPath("/unzipped")
        extractZipFile(zipFile, unzippedPath).shouldBeRight()

        // TEST
        val result = findHypermediaArtifacts(unzippedPath, listOf("index.html", "configuredName.html")).getOrNull()

        result shouldNotBe null
        result?.shouldHaveSize(1)
        result?.first()?.absolutePathString()?.shouldBe("/unzipped/build/reports/tests/test/index.html")
    }

    test("finds index files with configurable names in a tree of directories") {
        // PREP
        val fs = Jimfs.newFileSystem()
        val zipFile = fs.getPath("/zipfile.zip")
        loadLocalTestFileIntoVirtualFs(Path("dev/malteesch/artifakts/build_dir_with_junit_report.zip"), zipFile)
            .shouldBeRight()
        val unzippedPath = fs.getPath("/unzipped")
        extractZipFile(zipFile, unzippedPath).shouldBeRight()

        // TEST
        Files.move(
            unzippedPath.resolve("build/reports/tests/test/index.html"),
            unzippedPath.resolve("build/reports/tests/test/configuredName.html")
        )
        val result = findHypermediaArtifacts(unzippedPath, listOf("index.html", "configuredName.html")).getOrNull()

        result shouldNotBe null
        result?.shouldHaveSize(1)
        result?.first()?.absolutePathString()?.shouldBe("/unzipped/build/reports/tests/test/configuredName.html")
    }

    test("once index file is found it doesn't descend deeper into that directory") {
        // PREP
        val fs = Jimfs.newFileSystem()
        val zipFile = fs.getPath("/zipfile.zip")
        loadLocalTestFileIntoVirtualFs(
            Path("dev/malteesch/artifakts/dont_descend_dir_with_found_index_file.zip"),
            zipFile
        ).shouldBeRight()
        val unzippedPath = fs.getPath("/unzipped")
        extractZipFile(zipFile, unzippedPath).shouldBeRight()

        // TEST
        val result = findHypermediaArtifacts(unzippedPath, listOf("index.html")).onLeft(::println).getOrNull()

        result?.shouldHaveSize(2)
        result?.shouldContainAll(
            unzippedPath.resolve("dont_descend_dir_with_found_index_file/one/index.html"),
            unzippedPath.resolve("dont_descend_dir_with_found_index_file/two/index.html")
        )
        result?.shouldNotContain(
            unzippedPath.resolve("dont_descend_dir_with_found_index_file/two/three/index.html")
        )
    }
})
