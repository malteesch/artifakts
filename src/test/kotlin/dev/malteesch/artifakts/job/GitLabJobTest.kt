package dev.malteesch.artifakts.job

import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FunSpec
import io.ktor.http.*

class GitLabJobTest : FunSpec({

    context(::parseGitLabJobUrl.name) {

        test("parses valid GitLab job url") {
            val result = parseGitLabJobUrl(Url("https://gitlab.com/h-brs1/bachelor-thesis/-/jobs/4905433832"))

            result.shouldBeRight(
                GitLabJob(
                    "h-brs1/bachelor-thesis",
                    "4905433832",
                    Url("https://gitlab.com/h-brs1/bachelor-thesis/-/jobs/4905433832")
                )
            )
        }

        test("returns error result when trying to parse other url") {
            val result = parseGitLabJobUrl(Url("https://gitlab.com/h-brs1/bachelor-thesis/-/pipelines/973106916"))

            result.shouldBeLeft()
        }
    }
})
