package dev.malteesch.artifakts

import arrow.core.Either
import arrow.core.raise.either
import arrow.core.right
import com.google.common.jimfs.Jimfs
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.longs.shouldBeGreaterThan
import io.kotest.matchers.shouldNotBe
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.fileSize

class VirtualFsUtils : FunSpec({

    test("can write local test file to virtual fs") {
        val fs = Jimfs.newFileSystem()
        val zipFile = fs.getPath("/zipfile.zip")

        val result =
            loadLocalTestFileIntoVirtualFs(Path("dev/malteesch/artifakts/build_dir_with_junit_report.zip"), zipFile)

        result.shouldBeRight()
        zipFile.fileSize() shouldBeGreaterThan 0
    }
})

/**
 * @param localPath a path relative to the classloaders resource directory
 */
@OptIn(ExperimentalPathApi::class)
fun loadLocalTestFileIntoVirtualFs(localPath: Path, destPath: Path): Either<Throwable, Path> = either {
    arrow.core.raise.catch({
        val localTestFile: URL? =
            ZipFileExtractorTest::class.java.classLoader.getResource(localPath.toString())
        localTestFile shouldNotBe null

        val bytes: ByteArray = localTestFile?.readBytes() ?: ByteArray(0)
        return Files.write(destPath, bytes).right()
    }) { t -> raise(t) }
}
