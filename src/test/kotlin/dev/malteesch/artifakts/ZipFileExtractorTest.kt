package dev.malteesch.artifakts

import com.google.common.jimfs.Jimfs
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.paths.shouldExist
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path

@ExperimentalPathApi
class ZipFileExtractorTest : FunSpec({

    context("ZipFileExtractor") {
        test("extracts gradle build dir (one created from this project used as example)") {
            val fs = Jimfs.newFileSystem()
            val zipFile = fs.getPath("/zipfile.zip")

            loadLocalTestFileIntoVirtualFs(Path("dev/malteesch/artifakts/build_dir_with_junit_report.zip"), zipFile)
                .shouldBeRight()

            val unzippedPath = fs.getPath("/unzipped")
            val result = extractZipFile(zipFile, unzippedPath)

            result.shouldBeRight()
            unzippedPath.resolve("build/reports/tests").shouldExist()
        }
    }

    context("helper") {
    }
})
