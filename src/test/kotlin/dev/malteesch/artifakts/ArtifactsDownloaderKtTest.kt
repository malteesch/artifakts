package dev.malteesch.artifakts

import com.google.common.jimfs.Jimfs
import dev.malteesch.artifakts.job.GitLabJob
import io.kotest.assertions.all
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.common.ExperimentalKotest
import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.mockserver.MockServerListener
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.paths.shouldExist
import io.ktor.http.*
import org.mockserver.client.MockServerClient
import org.mockserver.model.HttpRequest
import org.mockserver.model.HttpResponse
import java.nio.file.Files

@ExperimentalKotest
class ArtifactsDownloaderKtTest : FunSpec({

    listener(MockServerListener(8080, 8081))

    beforeTest {
        val gitlabMockServer = MockServerClient("localhost", 8080)
        val gitlabCdnMockServer = MockServerClient("localhost", 8081)

        gitlabMockServer.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/api/v4/projects/some%2Fproject/jobs/4905433832/artifacts")
                .withHeader("Authorization", "Bearer token")
        ).respond(
            HttpResponse.response()
                .withStatusCode(302)
                .withHeader("Location", "http://localhost:8081/some/cdn/location")
        )

        gitlabCdnMockServer.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/some/cdn/location")
        ).respond(
            HttpResponse.response()
                .withStatusCode(200)
                .withBody("zip-file".toByteArray())
        )
    }

    test("downloads artifacts.zip") {

        val fs = Jimfs.newFileSystem()

        val destinationDir = fs.getPath("/tmp/artifakts")

        val result = downloadJobArtifacts(
            GitLabJob(
                "some/project",
                "4905433832",
                Url("http://localhost:8080/some/project/-/jobs/4905433832")
            ),
            destinationDir,
            "token"
        )

        all {
            result.shouldBeRight()
            fs.getPath("/tmp/artifakts/4905433832.zip").shouldExist()
        }
    }

    test("if file already exists the download is skipped") {

        val fs = Jimfs.newFileSystem()

        val destinationDir = fs.getPath("/tmp/artifakts")
        Files.createDirectories(destinationDir)
        Files.createFile(destinationDir.resolve("4905433832.zip"))

        val zipFile = fs.getPath("/tmp/artifakts/4905433832.zip")
        Files.write(zipFile, "existing-file-content".toByteArray())

        val result = downloadJobArtifacts(
            GitLabJob(
                "some/project",
                "4905433832",
                Url("http://localhost:8080/some/project/-/jobs/4905433832")
            ),
            destinationDir,
            "token"
        )

        all {
            result.shouldBeRight()
            Files.readAllBytes(zipFile).decodeToString() shouldBeEqual "existing-file-content"
        }
    }
})
