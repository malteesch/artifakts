package dev.malteesch.artifakts.config

import com.sksamuel.hoplite.yaml.YamlPropertySource
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.FunSpec

class ConfigLoaderTest : FunSpec({

    context("${::loadConfig}") {
        test("with existing and valid file returns config") {
            val result = loadConfig(
                listOf(
                    Source.Property(
                        YamlPropertySource(
                            """
                            instances:
                                gitlab.com: someToken
                            additionalIndexFileNames:
                                - configuredName.html
                            """.trimIndent()
                        )
                    )
                )
            )

            result.shouldBeRight(
                ConfigFile(
                    mapOf(Host("gitlab.com") to Token("someToken")),
                    listOf(IndexFileName("configuredName.html"))
                )
            )
        }

        test("without any config returns empty config file") {
            val result = loadConfig(emptyList())

            result.shouldBeRight(ConfigFile(emptyMap()))
        }
    }
})
