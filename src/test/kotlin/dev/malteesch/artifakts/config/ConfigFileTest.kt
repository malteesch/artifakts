package dev.malteesch.artifakts.config

import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.json.JsonPropertySource
import com.sksamuel.hoplite.yaml.YamlPropertySource
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull

class ConfigFileTest : FunSpec({

    test("can load multiple GitLab configs") {
        val config = ConfigLoaderBuilder.default()
            .addSource(
                YamlPropertySource(
                    """
                    instances:
                        gitlab.com: someToken
                        git.example.com: someOtherToken
                    """.trimIndent()
                )
            )
            .build()
            .loadConfigOrThrow<ConfigFile>()

        config.instances[Host("gitlab.com")]?.shouldBeEqual(Token("someToken"))
        config.instances[Host("git.example.com")]?.shouldBeEqual(Token("someOtherToken"))
    }

    test("also works with json") {
        val config = ConfigLoaderBuilder.default()
            .addSource(
                JsonPropertySource(
                    """
                    {
                        "instances": {
                            "gitlab.com": "someToken",
                            "git.example.com": "someOtherToken"
                        }
                    }
                    """.trimIndent()
                )
            )
            .build()
            .loadConfigOrThrow<ConfigFile>()

        config.instances[Host("gitlab.com")]?.shouldBeEqual(Token("someToken"))
        config.instances[Host("git.example.com")]?.shouldBeEqual(Token("someOtherToken"))
    }

    context("${ConfigFile::getToken}") {

        test("returns token string for existing host") {
            val subject = ConfigFile(mapOf(Host("gitlab.com") to Token("someToken")))

            subject.getToken(Host("gitlab.com"))?.shouldBeEqual("someToken")
        }

        test("returns null if no configured token exists") {
            val subject = ConfigFile(emptyMap())

            subject.getToken(Host("gitlab.com"))?.shouldBeNull()
        }
    }
})
