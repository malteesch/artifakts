package dev.malteesch.artifakts.commands

import com.github.ajalt.clikt.testing.CliktCommandTestResult
import com.github.ajalt.clikt.testing.test
import com.google.common.jimfs.Jimfs
import io.kotest.assertions.all
import io.kotest.common.ExperimentalKotest
import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.mockserver.MockServerListener
import io.kotest.matchers.longs.shouldBeGreaterThan
import io.kotest.matchers.paths.shouldExist
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.mockserver.client.MockServerClient
import org.mockserver.model.HttpRequest
import org.mockserver.model.HttpResponse
import java.nio.file.Files
import kotlin.io.path.fileSize

@ExperimentalKotest
class RootCommandTest : FunSpec({

    listener(MockServerListener(8080, 8081))

    beforeTest {
        val gitlabMockServer = MockServerClient("localhost", 8080)
        val gitlabCdnMockServer = MockServerClient("localhost", 8081)

        gitlabMockServer.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/api/v4/projects/some%2Fproject/jobs/4905433832/artifacts")
                .withHeader("Authorization", "Bearer token")
        ).respond(
            HttpResponse.response()
                .withStatusCode(302)
                .withHeader("Location", "http://localhost:8081/some/cdn/location")
        )

        val zipFileBytes: ByteArray =
            this::class.java.classLoader.getResource("dev/malteesch/artifakts/only_junit_report.zip")
                ?.readBytes()
                ?: "zip-file".toByteArray()

        gitlabCdnMockServer.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/some/cdn/location")
        ).respond(
            HttpResponse.response()
                .withStatusCode(200)
                .withBody(zipFileBytes)
        )
    }

    xtest("downloads zip file") {
        val fs = Jimfs.newFileSystem()
        val downloadDir = Files.createDirectories(fs.getPath("/tmp/artifakts"))
        val command = RootCommand(downloadDir)
        val result: CliktCommandTestResult =
            command.test("-t token http://localhost:8080/some/project/-/jobs/4905433832")

        val zipFile = downloadDir.resolve("4905433832.zip")
        all {
            result.statusCode shouldBe 0
            zipFile.shouldExist()
            zipFile.fileSize() shouldBeGreaterThan 0
        }
    }

    test("outputs error message if url is malformed") {
        val fs = Jimfs.newFileSystem()
        val downloadDir = Files.createDirectories(fs.getPath("/tmp/artifakts"))
        val command = RootCommand(downloadDir)
        val result: CliktCommandTestResult = command.test("-t token http://example.com")

        all {
            result.statusCode shouldBe 1
            result.stderr shouldContain "URL was not a valid GitLab job URL (http://example.com)"
        }
    }

    test("outputs error message if job is not found") {
        val fs = Jimfs.newFileSystem()
        val downloadDir = Files.createDirectories(fs.getPath("/tmp/artifakts"))
        val command = RootCommand(downloadDir)
        val result: CliktCommandTestResult =
            command.test("-t token http://localhost:8080/some/project/-/jobs/4905433831")

        all {
            result.statusCode shouldBe 1
            result.stderr shouldContain "No job found at url: http://localhost:8080/some/project/-/jobs/4905433831"
        }
    }

    xtest("extracts the downloaded zip file") {
        val fs = Jimfs.newFileSystem()
        val downloadDir = Files.createDirectories(fs.getPath("/tmp/artifakts"))
        val command = RootCommand(downloadDir)
        val result: CliktCommandTestResult =
            command.test("-t token http://localhost:8080/some/project/-/jobs/4905433832")

        val zipFile = downloadDir.resolve("4905433832.zip")
        val unzipped = downloadDir.resolve("4905433832")
        all {
            result.statusCode shouldBe 0
            zipFile.shouldExist()
            zipFile.fileSize() shouldBeGreaterThan 0

            unzipped.shouldExist()
        }
    }
})
