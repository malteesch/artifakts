# Artifakts

## Motivation
When using [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/) we often save artifacts of build jobs.
In some cases these are HTML reports that need to be extracted and opened/served.
This tool helps to ease this process.
Additionally, I wanted to try out and play around with some Kotlin libs. ;)

## Installation

### Requirements
- Java 21 or newer
- GitLab access token with `read_api` permissions

### Steps
- Clone the repository
- Run `gradlew distZip`
- copy ore move the resulting zip file from _build/distributions_ to some directory in your `$PATH`
- extract and optionally delete the zip file.
- create a symlink next to the binary in the extracted directory with your preferred name
  (I use `art`) `ln -s <extracted/dir>/bin/artifakts ./art`

## Config
You can pass your GitLab access token everytime.
A more convenient way is the config file.

Create a `.artifakts.[yaml|yml|json]` file in your users `$HOME` directory
and provide the token in the following structure:
```yaml
instances:
  gitlab.com: <your-token-here>
```
The key before the token is used to determine which one to use for your url
(if you're using this tool with multiple GitLab instances).

Sometimes the HTML reports do not use an _index.html_ file.
As this tool only looks for _index_ files you would need to provide additional ones via the config like so.
```yaml
additionalIndexFileNames:
  - karate-summary.html # the report of the karate test automation tool
```

## Usage
After everything is set up correctly just call the tool with a GitLab Job url.
These are expected to have the following form: `https://gitlab.com/your/group/your-repo/-/jobs/133935261`
```
Usage: art [<options>] <url>

Options:
  -t, --token, --access-token=<token>  Your GitLab access token
  -h, --help                           Show this message and exit
```
