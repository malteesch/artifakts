import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.detekt)
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.clikt)
    implementation(libs.mordant)
    implementation(libs.mordant.coroutines)
    implementation(libs.bundles.hoplite)
    implementation(libs.bundles.ktor.client)
    implementation(libs.bundles.ktor.server)
    implementation(libs.bundles.arrow)

    testImplementation(libs.bundles.kotest)
    testImplementation(libs.kotest.extensions.mockserver)
    testImplementation(libs.kotest.extensions.arrow)
    testImplementation(libs.jimfs)

    detektPlugins(libs.detekt.formatting)
    detektPlugins(libs.detekt.arrow)
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
    systemProperties("kotest.framework.classpath.scanning.autoscan.disable" to "true")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

application {
    mainClass.set("dev.malteesch.artifakts.ArtifaktsKt")
}

detekt {
    buildUponDefaultConfig = true
    allRules = false
    config.setFrom("$projectDir/config/detekt.yml")
}

tasks.check {
    dependsOn(tasks.detektTest)
}

tasks.withType<KotlinCompile>().configureEach {
    compilerOptions {
        freeCompilerArgs.add("-Xcontext-receivers")
    }
}
